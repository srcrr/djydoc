# Djydoc

Read Python Documentation within a Django Project.

So you're happily going along in development and you want to quickly view
documentation in a module. For python modules this is a piece of cake.

However, when you try to find out more information about a Django app, you may
have come across an error like this...

![error-1](https://gitlab.com/srcrr/djydoc/-/raw/0.1.0-dev/docs/images/error-1.png)

Or like this...

![error-2](https://gitlab.com/srcrr/djydoc/-/raw/0.1.0-dev/docs/images/error-2.png)

And you may have done this...

![scream](https://images.pexels.com/photos/3799830/pexels-photo-3799830.jpeg?auto=compress&cs=tinysrgb&h=640&w=426)

But after installing `djydoc` now you can do this:

![relax](https://images.pexels.com/photos/846080/pexels-photo-846080.jpeg?auto=compress&cs=tinysrgb&h=640&w=426)


> Djydoc aims to be a simple drop-in replacement as a `manage.py` command
> for pydoc so that you can view
> Python documentation without having to specify `DJANGO_SETTINGS_MODULE` or
> run django.setup(). Djydoc does that for you.

## Summary

  - [Getting Started](#getting-started)
  - [Runing the tests](#running-the-tests)
  - [Deployment](#deployment)
  - [Built With](#built-with)
  - [Contributing](#contributing)
  - [Versioning](#versioning)
  - [Authors](#authors)
  - [License](#license)
  - [Acknowledgments](#acknowledgments)

## Getting Started

These instructions will get you a copy of the project up and running on
your local machine for development and testing purposes. See deployment
for notes on how to deploy the project on a live system.

### Prerequisites

- Django 2.2.4+
- Poetry (optional, to help easily install)

### Installing

#### Option 1 : PIP

    $ pip install djydoc

#### Option 2: Development install via poetry

    $ git clone <git_url>
    $ cd djydoc
    $ poetry install

#### Option 3: use as a Django app

Just follow option 2, but don't install with poetry

#### Options 1, 2, or 3: Setup

Add `'djydoc'` to `INSTALLED_APPS` in `settings`:

```python
INSTALLED_APPS += [
  'djydoc'
]
```

## Usage

Once installed djydoc should be available via the `./manage.py` command.

The intention is for this to be a drop-in replacement for pydoc, so all the
commands should work the same (currently `-w` is not implemented):

    $ ./manage.py djydoc django.contrib.auth.models.User

    [ documentation shown ]

    $ ./manage.py djydoc -p 8080
    Server ready at http://0.0.0.0:8080/
    Server commands: [b]rowser, [q]uit
    server>


## Running the tests

Testing TBD.


## Deployment

Follow common sense django deployment practices.

## Built With

  - [Contributor Covenant](https://www.contributor-covenant.org/) - Used
    for the Code of Conduct
  - [Creative Commons](https://creativecommons.org/) - Used to choose
    the license

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code
of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions
available, see the [tags on this
repository](https://github.com/PurpleBooth/a-good-readme-template/tags).

## Authors

  - **Jordan H.** - *Code Author* -
    [SrcRr](https://gitlab.com/srcrr)


## License

This project is licensed under [MIT](LICENSE.md).

## Acknowledgments

TBD
